<!DOCTYPE html>
<head>
    <title>Opdracht week 5b</title>
</head>
<body>
<?php

// We gebruiken 3 variabelen waarin we de reeds ingevuld waardes opslaan
// Initieel zijn deze leeg
$email = "";
$onderwerp = "";
$bericht = "";

// En we hebben een variabele waarmee we bijhouden of we kunnen versturen
// of dat we het formulier moeten laten zien. Initieel is deze FALSE.
$versturen = FALSE;

// Als er op de Verstuur knop is geklikt (maw het formulier is verzonden)...
if (isset($_GET["verstuur"])) {
    // ... dan vullen we de variabelen met de waardes die de gebruiker heeft ingevuld
    $email = $_GET["email"];
    $onderwerp = $_GET["onderwerp"];
    $bericht = $_GET["bericht"];

    // Daarna kijken we of één van de waardes leeg is...
    if ($email == "" || $onderwerp == "" || $bericht == "") {
        // ... en in dat geval tonen we een melding
        print("Zorg dat alle velden ingevuld zijn!!");
    } else {
        // ... anders zetten we de $versturen variabele op TRUE zodat het bericht wordt verstuurd
        $versturen = TRUE;
    }
}

// Als versturen TRUE is ...
if ($versturen) {
    // ... dan zijn blijkbaar alle velden gevuld en tonen we de melding
    print ("Bericht wordt verzonden");
} else {
    // ... anders zijn nog niet alle velden gevuld en tonen we het formulier.
    //     Hierin maken we gebruik van de variabelen in de value="" van de invoervelden
    //     om de reeds ingevulde waardes te tonen... of de initiele waarde "" als het formulier
    //     nog niet is verzonden.
    ?>
    <form method="get" action="Opdracht%204.php">
        E-mailadres: <input type="text" name="email" value="<?php print($email); ?>"><br>
        Onderwerp: <input type="text" name="onderwerp" value="<?php print($onderwerp); ?>"><br>
        Bericht: <textarea name="bericht" rows="4"><?php print($bericht); ?></textarea><br>
        <input type="submit" name="verstuur" value="Verstuur bericht">
    </form>
    <?php
}
?>

</body>
</html>