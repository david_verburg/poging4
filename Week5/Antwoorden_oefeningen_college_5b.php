<!DOCTYPE html>
    <head>
        <title>Opdracht week 5b</title>
    </head>
    <body>
        <?php
        if (isset($_GET["verstuur"])) {
            if ($_GET["email"] == "" || $_GET["onderwerp"] == "" || $_GET["bericht"] == "") {
                print("<h2>Zorg dat de verplichte velden ingevuld zijn!!</h2>");
                ?><form method="get" action="Antwoorden_oefeningen_college_5b.php">
                    E-mailadres: <input type="text" name="email" value="<?php print($_GET['email']) ?>">
                <?php
                if ($_GET["email"] == "") {
                    print("Verplicht!");
                }
                ?>
                <br>
                Onderwerp: <input type="text" name="onderwerp" value="<?php print($_GET['onderwerp']) ?>">
                <?php
                if ($_GET["onderwerp"] == "") {
                    print("Verplicht!");
                }
                ?>
                <br>
                Bericht: <textarea name="bericht" rows="4" ><?php print($_GET['bericht']) ?></textarea>
                <?php
                if ($_GET["bericht"] == "") {
                    print("Verplicht!");
                }
                ?>
                <br>
                <input type="submit" name="verstuur" value="Verstuur bericht">
                </form><?php
            } else {
                print("Bericht wordt verzonden!!!");
            }
        } else {
            ?>
            <form method="get" action="Antwoorden_oefeningen_college_5b.php">
                E-mailadres: <input type="text" name="email"><br>
                Onderwerp: <input type="text" name="onderwerp"><br>
                Bericht: <textarea name="bericht" rows="4"></textarea><br>
                <input type="submit" name="verstuur" value="Verstuur bericht">
            </form>
            <?php
        }
        ?>
    </body>
</html>
