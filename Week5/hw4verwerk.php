<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<?php
$aantalkeer = $_GET["aantal"];
if ($aantalkeer < 1 || empty($aantalkeer)) {
    print("Aantal moet ingevuld zijn! <br/>");
} else {
    for ($aantal = 1; $aantal <= $aantalkeer; $aantal++) {
        $waarde = rand(1, 6);
        print("Worp " . $aantal . " : " . $waarde . "<br/>");
    }
}
?>
</body>
</html>
