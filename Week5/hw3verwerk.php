<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<?php
    $rapportcijfer = (int) $_GET["rapportcijfer"];
    $leeftijd = $_GET["leeftijd"];
    $algemeindruk = $_GET["indruk"];
    if (empty($rapportcijfer) || empty($leeftijd)) {
        print("Rapportcijfer en leeftijd zijn verplicht! <br/>");
        print("<a href=hw3.php>Terug naar het formulier</a>");
    } elseif ($rapportcijfer < 1 || $rapportcijfer > 10 || $leeftijd < 16 || $leeftijd > 120) {
        print("rapportcijfer moet tussen de 1 en 10 liggen. Leeftijd moet tussen de 16 en 120 liggen! <br/>");
        print("<a href=hw3.php>Terug naar het formulier</a>");
    } elseif ((($rapportcijfer == 9 || $rapportcijfer == 10) && $algemeindruk != "Super") || (($rapportcijfer > 5 && $rapportcijfer < 9) && $algemeindruk != "Goed") ||(($rapportcijfer == 4 || $rapportcijfer == 5) && $algemeindruk != "Matig") ||(($rapportcijfer > 0 && $rapportcijfer < 4) && $algemeindruk != "Slecht"))
    {
        print("Dus je wilde beweren dat " . $_GET["rapportcijfer"] . " gewoon " . $algemeindruk . " is. <br/>");
        print("<a href=hw3.php>Terug naar het formulier</a>");
    } else {
        print("Je bent volgens je eigen zeggen " . $leeftijd . " jaar oud. <br/>");
        print("Ik ben het met je eens dat " . $rapportcijfer . " gewoon " . $algemeindruk . " is. <br/>");
        print("<a href=hw3.php>Terug naar het formulier</a>");
    }
    ?>
</body>