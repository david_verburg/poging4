<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    <style>
        div {float: left;}
        div.rechts {float: right;}
    </style>
</head>
<body>
Beste student je hebt meegedaan.... etc.<br><br>
<form method="get" action="hw3verwerk.php">
    <div>
        Rapportcijfer: <input type="text" name="rapportcijfer"><br>
        Leeftijd: <input type="text" name="leeftijd"><br>
        <input type="submit" value="Verstuur!">
    </div>
    <div class = "rechts">
        <input type="radio" name="indruk" value="Super" checked="1">Super<br>
        <input type="radio" name="indruk" value="Goed">Goed<br>
        <input type="radio" name="indruk" value="Matig">Matig<br>
        <input type="radio" name="indruk" value="Slecht">Slecht<br>
    </div>
</form>
</body>
</html>
